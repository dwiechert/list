import 'dart:io';

import "package:list/info.dart";

void main(List<String> arguments) {
  print('Arguments: $arguments');
  final entity = Directory.current;
  print('Current Directory: ${entity.path}');
  final infos = getFileInfo(entity);
  printFileInfos(infos);
}